#include <message_handler.h>

namespace icsl
{

MessageHandler::MessageHandler()
{
	nh_ = new ros::NodeHandle();

	pos_sp_sub_ = nh_->subscribe<PosSp>( "gcs/setpoint_raw/position", 10, 
		 &MessageHandler::pos_sp_cb, this);
	pose_sub_ = nh_->subscribe<PoseStamped>( "mavros/local_position/pose", 10, 
		 &MessageHandler::pose_cb, this);
	twist_sub_ = nh_->subscribe<TwistStamped>( "mavros/local_position/velocity", 10, 
			&MessageHandler::twist_cb, this);
	odometry_sub_ = nh_->subscribe<Odometry>( "msf_core/odometry", 10, 
			&MessageHandler::odometry_cb, this);

	att_sp_pub_ = nh_->advertise<AttSp>( "mavros/setpoint_raw/attitude", 1 );
	
	ctrl_ = new Controller();

	ros::NodeHandle pnh = ros::NodeHandle(ros::this_node::getName().c_str());
	double k_x, k_y, k_z;
	if( pnh.getParam("k_r_x", k_x) && 
		pnh.getParam("k_r_y", k_y) && 
		pnh.getParam("k_r_z", k_z) )
	{
		ctrl_->setPositionGains(k_x, k_y, k_z);
	}
	else
	{
		ROS_WARN("position gain is not set, set to defaults");
		ctrl_->setPositionGains(1.0, 1.0, 1.0);
	}
	if( pnh.getParam("k_v_x", k_x) && 
		pnh.getParam("k_v_y", k_y) && 
		pnh.getParam("k_v_z", k_z) )
	{
		ctrl_->setVelocityGains(k_x, k_y, k_z);
	}
	else
	{
		ROS_WARN("velocity gain is not set, set to defaults");
		ctrl_->setVelocityGains(1.0, 1.0, 1.0);
	}
	if( pnh.getParam("k_R_x", k_x) && 
		pnh.getParam("k_R_y", k_y) && 
		pnh.getParam("k_R_z", k_z) )
	{
		ctrl_->setAttitudeGains(k_x, k_y, k_z);
	}
	else
	{
		ROS_WARN("attitude gain is not set, set to defaults");
		ctrl_->setAttitudeGains(1.0, 1.0, 1.0);
	}
	if( pnh.getParam("k_r_int_x", k_x) && 
		pnh.getParam("k_r_int_y", k_y) && 
		pnh.getParam("k_r_int_z", k_z) )
	{
		ctrl_->setPositionIntegralGains(k_x, k_y, k_z);
	}
	else
	{
		ROS_WARN("position integral gain is not set, set to defaults");
		ctrl_->setPositionIntegralGains(0.0, 0.0, 0.0);
	}
	if( pnh.getParam("k_v_int_x", k_x) && 
		pnh.getParam("k_v_int_y", k_y) && 
		pnh.getParam("k_v_int_z", k_z) )
	{
		ctrl_->setVelocityIntegralGains(k_x, k_y, k_z);
	}
	else
	{
		ROS_WARN("velocity integral gain is not set, set to defaults");
		ctrl_->setVelocityIntegralGains(0.0, 0.0, 0.0);
	}
	if( pnh.getParam("k_r_diff_x", k_x) && 
		pnh.getParam("k_r_diff_y", k_y) && 
		pnh.getParam("k_r_diff_z", k_z) )
	{
		ctrl_->setPositionDifferentialGains(k_x, k_y, k_z);
	}
	else
	{
		ROS_WARN("position differential gain is not set, set to defaults");
		ctrl_->setPositionDifferentialGains(0.0, 0.0, 0.0);
	}
	if( pnh.getParam("k_v_diff_x", k_x) && 
		pnh.getParam("k_v_diff_y", k_y) && 
		pnh.getParam("k_v_diff_z", k_z) )
	{
		ctrl_->setVelocityDifferentialGains(k_x, k_y, k_z);
	}
	else
	{
		ROS_WARN("velocity differential gain is not set, set to defaults");
		ctrl_->setVelocityDifferentialGains(0.0, 0.0, 0.0);
	}

	pos_sp_flag_ = false;
	pose_flag_ = false;
	twist_flag_ = false;

	att_sp_.type_mask = AttSp::IGNORE_ATTITUDE;
}

MessageHandler::~MessageHandler()
{
	delete nh_;
	delete ctrl_;
}

void MessageHandler::publish( double dt )
{
	if( pos_sp_flag_ && pose_flag_ && twist_flag_ )
	{
		if( ctrl_->compute( att_sp_, dt ) )
		{
			att_sp_.header.stamp = ros::Time::now();
			att_sp_pub_.publish( att_sp_ );
			att_sp_.header.seq = att_sp_.header.seq + 1;
		}
	}
	else
	{
	//	ROS_WARN("current state and setpoints are not callbacked");
	}
}

void MessageHandler::pos_sp_cb( const PosSp::ConstPtr& msg )
{
	ctrl_->setPosSp( *msg );
	pos_sp_flag_ = true;
}
void MessageHandler::pose_cb( const PoseStamped::ConstPtr& msg )
{
	ctrl_->setCurPose( msg->pose );
	pose_flag_ = true;
}
void MessageHandler::twist_cb( const TwistStamped::ConstPtr& msg )
{
	ctrl_->setCurTwist( msg->twist );
	twist_flag_ = true;
}
void MessageHandler::odometry_cb( const Odometry::ConstPtr& msg )
{
	ctrl_->setCurPose( msg->pose.pose );
	ctrl_->setCurTwist( msg->twist.twist );
	pose_flag_ = true;
	twist_flag_ = true;
}

}
