#include <message_handler.h>

int main(int argc, char **argv)
{
	ros::init(argc, argv, "controller");
	icsl::MessageHandler handle;

	ros::Rate r(200);
	while( ros::ok() )
	{
		handle.publish( 1.0 / 200.0 );
		r.sleep();
		ros::spinOnce();
	}
	return 0;
}
