#include <controller.h>

namespace icsl
{

Controller::Controller()
{
	r_.setZero();
	v_.setZero();
	q_.setIdentity();
	r_d_.setZero();
	v_d_.setZero();
	psi_d_ = 0.0;
	k_r_.setZero();
	k_v_.setZero();
	k_R_.setZero();
	int_e_r_.setZero();
	int_e_v_.setZero();
	prev_e_r_.setZero();
	prev_e_v_.setZero();

	m_ = 1.5; // mass of our drone
	g_ = -9.81;
	e3_.setZero(); e3_(2,0) = 1.0;
}

Controller::~Controller()
{
}

bool Controller::compute( AttSp &att_sp, double dt )
{
	Eigen::Matrix<double,3,3> R = q_.toRotationMatrix();
	
	//// position control
	Eigen::Matrix<double,3,1> e_r = r_ - r_d_; // position error
	Eigen::Matrix<double,3,1> e_v = R*(v_ - v_d_);  // velocity error in inertial frame

	if( mode_ & HOLD )
	{
		e_r(0,0) = 0.0;
		e_r(1,0) = 0.0;
	}
	else if( mode_ & VELOCITY )
	{
		e_r(0,0) = 0.0;
		e_r(1,0) = 0.0;
		e_r(2,0) = 0.0;
	}
	Eigen::Matrix<double,3,1> b3_d;
	b3_d = (-1.0)*( k_r_.asDiagonal()*e_r 
				  + k_v_.asDiagonal()*e_v 
				  + k_r_int_.asDiagonal()*int_e_r_
				  + k_v_int_.asDiagonal()*int_e_v_
				  + g_*e3_);

	
	double f = m_ * b3_d.norm();

	// attitude control
	b3_d *= 1.0 / b3_d.norm();
	Eigen::Matrix<double,3,1> bc_d;
	bc_d(0,0) = cos(psi_d_); // TODO : for yaw_rate control, this must be current yaw angle
	bc_d(1,0) = sin(psi_d_);
	bc_d(2,0) = 0.0;
	Eigen::Matrix<double,3,1> b2_d = b3_d.cross(bc_d);
	Eigen::Matrix<double,3,1> b1_d = b2_d.cross(b3_d);
	
	Eigen::Matrix<double,3,3> R_d;
	R_d.block<3,1>(0,0) = b1_d;
	R_d.block<3,1>(0,1) = b2_d;
	R_d.block<3,1>(0,2) = b3_d;

	Eigen::Matrix<double,3,3> A = R.transpose()*R_d;
	Eigen::Matrix<double,3,3> R_diff = 0.5*(A.transpose() - A);
	
	Eigen::Matrix<double,3,1> e_R;
	e_R(0,0) = R_diff(2,1);
	e_R(1,0) = R_diff(0,2);
	e_R(2,0) = R_diff(1,0);

	Eigen::Matrix<double,3,3> M = A.trace() * Eigen::Matrix<double,3,3>::Identity() - A;
	// TODO : currently yaw rate control is not supported
	Eigen::Matrix<double,3,1> w_d = (-1.0)*M.inverse()*(k_R_.asDiagonal()*e_R);

	// TODO : return false when control inputs are too large!
	// set msg (without modifying type masks)
	Eigen::Quaternion<double> q_d = R2q( R_d );
	att_sp.orientation.w = q_d.w(); 
	att_sp.orientation.x = q_d.x();
	att_sp.orientation.y = q_d.y();
	att_sp.orientation.z = q_d.z();
	att_sp.body_rate.x = w_d(0,0);
	att_sp.body_rate.y = -w_d(1,0); // only compatible with our px4
	att_sp.body_rate.z = -w_d(2,0); // only compatible with our px4
	att_sp.thrust = f / 100.0; // only compatible with our px4

	// error intrgration
	int_e_r_ += dt*e_r;
	int_e_v_ += dt*e_v;
	
	// previous error
	prev_e_r_ = e_r;
	prev_e_v_ = e_v;

	return true;
}

Eigen::Quaternion<double> Controller::R2q( Eigen::Matrix<double,3,3> R )
{
    double R11 = R(0,0);
    double R12 = R(0,1);
    double R13 = R(0,2);
    double R21 = R(1,0);
    double R22 = R(1,1);
    double R23 = R(1,2);
    double R31 = R(2,0);
    double R32 = R(2,1);
    double R33 = R(2,2);

	Eigen::Matrix<double,4,4> T = Eigen::Matrix<double,4,4>::Ones();
    T(1,1) = -1.0;
    T(1,2) = -1.0;
    T(2,0) = -1.0;
    T(2,2) = -1.0;
    T(3,0) = -1.0;
    T(3,1) = -1.0;

	Eigen::Matrix<double,4,1> diag;
    diag(0,0) = R11;
    diag(1,0) = R22;
    diag(2,0) = R33;
    diag(3,0) = 1.0;


	Eigen::Matrix<double,4,1> sq = 0.25*(T*diag);
    double q0 = sqrt(sq(0,0));
    double q1 = sqrt(sq(1,0));
    double q2 = sqrt(sq(2,0));
    double q3 = sqrt(sq(3,0));

    if( (q0 >= q1) && (q0 >= q2) && (q0 >= q3) )
    {
        q1 = copysign(q1, R32-R23);
        q2 = copysign(q2, R13-R31);
        q3 = copysign(q3, R21-R12);
    }
    else if( (q1 >= q0) && (q1 >= q2) && (q1 >= q3) )
    {
        q0 = copysign(q0, R32-R23);
        q2 = copysign(q2, R21+R12);
        q3 = copysign(q3, R13+R31);
    }
    else if( (q2 >= q0) && (q2 >= q1) && (q2 >= q3) )
    {
        q0 = copysign(q0, R13-R31);
        q1 = copysign(q1, R21+R12);
        q3 = copysign(q3, R32+R23);
    }
    else if( (q3 >= q0) && (q3 >= q1) && (q3 >= q2) )
    {
        q0 = copysign(q0, R21-R12);
        q1 = copysign(q1, R31+R13);
        q2 = copysign(q2, R32+R23);
    }

	Eigen::Quaternion<double> q;
    q.w() = q0;
    q.x() = q1;
    q.y() = q2;
    q.z() = q3;

    return q;
}

void Controller::setPosSp( PosSp pos_sp )
{
	// get current control mode
	mode_ = pos_sp.type_mask;

	// get position setpoint
	r_d_(0,0) = pos_sp.position.x;	
	r_d_(1,0) = pos_sp.position.y;	
	r_d_(2,0) = pos_sp.position.z;	

	// get velocity setpoint
	v_d_(0,0) = pos_sp.velocity.x;
	v_d_(1,0) = pos_sp.velocity.y;
	v_d_(2,0) = pos_sp.velocity.z;

	// get yaw setpoint
	psi_d_ = pos_sp.yaw;
	dpsi_d_ = pos_sp.yaw_rate;
}
void Controller::setCurPose( Pose pose )
{
	// get current position
	r_(0,0) = pose.position.x;
	r_(1,0) = pose.position.y;
	r_(2,0) = pose.position.z;

	// get current attitude
	q_.w() = pose.orientation.w;
	q_.x() = pose.orientation.x;
	q_.y() = pose.orientation.y;
	q_.z() = pose.orientation.z;
}
void Controller::setCurTwist( Twist twist )
{
	// get current velocity
	v_(0,0) = twist.linear.x;
	v_(1,0) = twist.linear.y;
	v_(2,0) = twist.linear.z;
}
void Controller::setPositionGains(double x, double y, double z)
{
	k_r_(0,0) = x;
	k_r_(1,0) = y;
	k_r_(2,0) = z;
}
void Controller::setVelocityGains(double x, double y, double z)
{
	k_v_(0,0) = x;
	k_v_(1,0) = y;
	k_v_(2,0) = z;
}
void Controller::setAttitudeGains(double x, double y, double z)
{
	k_R_(0,0) = x;
	k_R_(1,0) = y;
	k_R_(2,0) = z;
}
void Controller::setPositionIntegralGains(double x, double y, double z)
{
	k_r_int_(0,0) = x;
	k_r_int_(1,0) = y;
	k_r_int_(2,0) = z;
}
void Controller::setVelocityIntegralGains(double x, double y, double z)
{
	k_v_int_(0,0) = x;
	k_v_int_(1,0) = y;
	k_v_int_(2,0) = z;
}
void Controller::setPositionDifferentialGains(double x, double y, double z)
{
	k_r_diff_(0,0) = x;
	k_r_diff_(1,0) = y;
	k_r_diff_(2,0) = z;
}
void Controller::setVelocityDifferentialGains(double x, double y, double z)
{
	k_v_diff_(0,0) = x;
	k_v_diff_(1,0) = y;
	k_v_diff_(2,0) = z;
}

}
