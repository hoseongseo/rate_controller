#ifndef CONTROLLER
#define CONTROLLER

#include <Eigen/Dense>
#include <mavros_msgs/PositionTarget.h>
#include <mavros_msgs/AttitudeTarget.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Twist.h>

#include "control_modes.h"

typedef mavros_msgs::PositionTarget PosSp;
typedef mavros_msgs::AttitudeTarget AttSp;
typedef geometry_msgs::Pose Pose;
typedef geometry_msgs::Twist Twist;

namespace icsl
{

class Controller
{
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	Controller();
	~Controller();

	bool compute( AttSp &att_sp, double dt );
	void setPosSp( PosSp pos_sp );
	void setCurPose( Pose pose );
	void setCurTwist( Twist twist );
	void setPositionGains( double, double, double );
	void setVelocityGains( double, double, double );
	void setAttitudeGains( double, double, double );
	void setPositionIntegralGains( double, double, double );
	void setVelocityIntegralGains( double, double, double );
	void setPositionDifferentialGains( double, double, double );
	void setVelocityDifferentialGains( double, double, double );

private:
	// current states
	Eigen::Matrix<double,3,1> r_;
	Eigen::Matrix<double,3,1> v_;
	Eigen::Quaternion<double> q_;

	// setpoints
	Eigen::Matrix<double,3,1> r_d_;
	Eigen::Matrix<double,3,1> v_d_;
	double psi_d_;
	double dpsi_d_;
	int mode_;
	int mode_prev_;

	// error integration
	Eigen::Matrix<double,3,1> int_e_r_;
	Eigen::Matrix<double,3,1> int_e_v_;

	// previous error
	Eigen::Matrix<double,3,1> prev_e_r_;
	Eigen::Matrix<double,3,1> prev_e_v_;

	// gains
	Eigen::Matrix<double,3,1> k_r_;
	Eigen::Matrix<double,3,1> k_v_;
	Eigen::Matrix<double,3,1> k_R_;
	Eigen::Matrix<double,3,1> k_r_int_;
	Eigen::Matrix<double,3,1> k_v_int_;
	Eigen::Matrix<double,3,1> k_r_diff_;
	Eigen::Matrix<double,3,1> k_v_diff_;

	// others
	double m_;
	double g_;
	Eigen::Matrix<double,3,1> e3_;

	// utility function
	Eigen::Quaternion<double> R2q( Eigen::Matrix<double,3,3> R );	
};

}
#endif
