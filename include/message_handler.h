#ifndef MESSAGE_HANDLER
#define MESSAGE_HANDLER

#include <ros/ros.h>
#include <mavros_msgs/PositionTarget.h>
#include <mavros_msgs/AttitudeTarget.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include <nav_msgs/Odometry.h>

#include <controller.h>

typedef mavros_msgs::PositionTarget PosSp;
typedef mavros_msgs::AttitudeTarget AttSp;
typedef geometry_msgs::PoseStamped PoseStamped;
typedef geometry_msgs::TwistStamped TwistStamped;
typedef nav_msgs::Odometry Odometry;

namespace icsl
{

class MessageHandler
{
public:
	MessageHandler();
	~MessageHandler();
	void publish( double dt );

private:
	Controller* ctrl_;

	ros::NodeHandle* nh_;
	ros::Subscriber pos_sp_sub_;
	ros::Subscriber pose_sub_;
	ros::Subscriber twist_sub_;
	ros::Subscriber odometry_sub_;
	ros::Publisher att_sp_pub_;

	AttSp att_sp_;

	void pos_sp_cb(const PosSp::ConstPtr& msg);
	void pose_cb(const PoseStamped::ConstPtr& msg);
	void twist_cb(const TwistStamped::ConstPtr& msg);
	void odometry_cb(const Odometry::ConstPtr& msg);
	bool pos_sp_flag_;
	bool pose_flag_;
	bool twist_flag_;
};

}

#endif
